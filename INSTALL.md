# txt2unix: Building and Installing

## Prerequisites

Besides a C compiler, building `txt2unix` has the following dependencies.

1. [sharness](https://github.com/chriscool/sharness) to run unit tests (optional but recommended)
2. [ronn](https://github.com/rtomayko/ronn) to generate the manpage
3. Python 3 to generate unit tests (optional)

```
sudo apt install python3 sharness ronn
```

## Build

Build using defaults.

```
make
```

Optionally, you can set various build options.

```
make CC=clang CFLAGS="-g -O0"
```

## Test

The `test` target executes the [Test Anything Protocol (TAP)](https://testanything.org/) unit tests via `prove` and `sharness`.

```
$ make test
SHARNESS_TEST_SRCDIR=/usr/share/sharness TXT2UNIX=/path/to/txt2unix prove -v test/manual.t
test/manual.t .. 
ok 1 - txt2unix -a: bad option negative test
ok 2 - txt2unix -b: bad option negative test
ok 3 - txt2unix -c: bad option negative test
ok 4 - txt2unix < corpus/nix.txt
ok 5 - txt2unix < corpus/win.txt
...
ok 20 - txt2unix < corpus/mac_oneemptyline.txt
# passed all 20 test(s)
1..20
ok
All tests successful.
Files=1, Tests=20,  0 wallclock secs ( 0.02 usr  0.00 sys +  0.05 cusr  0.01 csys =  0.08 CPU)
Result: PASS
SHARNESS_TEST_SRCDIR=/usr/share/sharness TXT2UNIX=/path/to/txt2unix prove -v test/auto.t
test/auto.t .. 
ok 1 - txt2unix automated test #1: sin -n
ok 2 - txt2unix automated test #1: con -n
ok 3 - txt2unix automated test #2: sin -n
...
ok 200 - txt2unix automated test #100: con -n
# passed all 200 test(s)
1..200
ok
All tests successful.
Files=1, Tests=200,  0 wallclock secs ( 0.05 usr  0.00 sys +  0.65 cusr  0.13 csys =  0.83 CPU)
Result: PASS
```

## Install

The default destination path is `/usr/local` to install system-wide.
The `install` target will place the `txt2unix` binary under `bin/` and the manpage under `share/man/man1/`.

```
sudo make install
```

Or you can install it locally.

```
make install DESTDIR=$HOME/.local
```
