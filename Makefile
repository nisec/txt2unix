ifneq ("$(wildcard /usr/share/sharness/sharness.sh)","")
SHARNESS := /usr/share/sharness
endif

ifneq ("$(wildcard /usr/local/share/sharness/sharness.sh)","")
SHARNESS := /usr/local/share/sharness
endif

ifneq ("$(wildcard $HOME/.local/share/sharness/sharness.sh)","")
SHARNESS := $HOME/.local/share/sharness
endif

DESTDIR ?= /usr/local
CFLAGS ?= -march=native -O3 -fomit-frame-pointer

txt2unix: txt2unix.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ $<

txt2unix.1: README.md
	ronn < $< > $@

test/auto.t: test/txt2unix.py
	python3 $< -t > $@

test: txt2unix test/manual.t test/auto.t
	SHARNESS_TEST_SRCDIR=$(SHARNESS) TXT2UNIX=$(CURDIR)/txt2unix prove -v test/manual.t
	SHARNESS_TEST_SRCDIR=$(SHARNESS) TXT2UNIX=$(CURDIR)/txt2unix prove -v test/auto.t

install: txt2unix txt2unix.1
	mkdir -p $(DESTDIR)/bin
	mkdir -p $(DESTDIR)/share/man/man1
	install -m 755 txt2unix $(DESTDIR)/bin
	install -m 644 txt2unix.1 $(DESTDIR)/share/man/man1

clean:
	rm -f txt2unix txt2unix.1

.PHONY: clean test install
