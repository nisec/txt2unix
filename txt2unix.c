/**
 * MIT License
 *
 * Copyright (c) 2022 Billy Bob Brumley, D.Sc. (Tech.)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <getopt.h>

/* carriage return and line feed */
#define CR (0x0D)
#define LF (0x0A)

/* possible states */
typedef enum {
    READ,
    EMITREAD,
    EMIT
} state_t;

int main(int argc, char *argv[]) {
    int opt;
    char ci = ~EOF, co = ~LF, cl = 0;
    state_t state = READ; /* state machine */

    /* parse CLI options */
    while ((opt = getopt(argc, argv, "n")) != -1) {
        switch (opt) {
            case 'n': /* optionally ensure output ends in LF */
                cl = LF;
                break;
            case '?': /* unrecognized option */
            default:  /* error */
                return 1;
        }
    }

    while (ci != EOF) {
        switch (state) {
            case READ:
                if ((ci = getchar()) == CR)
                    state = EMITREAD;
                else
                    state = EMIT;
                break;
            case EMITREAD:
                if ((co = putchar(LF)) == EOF)
                    return 1;
                if ((ci = getchar()) == CR)
                    state = EMITREAD;
                else if (ci == LF)
                    state = READ;
                else
                    state = EMIT;
                break;
            case EMIT:
                if ((co = putchar(ci)) == EOF)
                    return 1;
                state = READ;
                break;
            default:
                return 1;
        }
    }

    /* optionally ensure output ends in LF */
    if (cl == LF && co != LF && putchar(LF) == EOF)
        return 1;

    return 0;
}
