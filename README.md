# txt2unix(1) -- normalize text file line endings

## SYNOPSIS

`txt2unix` [_OPTION_]

## DESCRIPTION

The utility `txt2unix` reads from standard input (_stdin_) and writes to standard output (_stdout_).
It copies the input to the output, but replaces carriage returns with line feeds.
Except in the carriage return + line feed case, which it replaces with a single line feed.

This helps to normalize the line endings in text files to UNIX line endings.

I hesitate to call it "converting" because that might imply a priori knowledge about the input line endings.
In contrast to utilities like `dos2unix`, `unix2dos`, `mac2unix`, `unix2mac`, `d2u`, `u2d`,
or various `tr` / `perl` / `sed` (often incorrect / non-portable) oneliners you might find on StackOverflow,
`txt2unix` makes no assumptions about the input.
This makes `txt2unix` particularly well-suited for inputs with unknown or possibly mixed line endings.

### LINE ENDINGS 101

UNIX line endings are the line feed character (`0x0A \n`).

```
$ xxd -g1 corpus/nix.txt
00000000: 74 68 65 0a 71 75 69 63 6b 0a 62 72 6f 77 6e 0a  the.quick.brown.
00000010: 66 6f 78 0a                                      fox.
```

WINDOWS line endings are the carriage return character (`0x0D \r`) followed by the line feed character.

```
$ xxd -g1 corpus/win.txt
00000000: 74 68 65 0d 0a 71 75 69 63 6b 0d 0a 62 72 6f 77  the..quick..brow
00000010: 6e 0d 0a 66 6f 78 0d 0a                          n..fox..
```

Legacy MAC line endings are the carriage return character.

```
$ xxd -g1 corpus/mac.txt
00000000: 74 68 65 0d 71 75 69 63 6b 0d 62 72 6f 77 6e 0d  the.quick.brown.
00000010: 66 6f 78 0d                                      fox.
```

Did you know files can have mixed line endings?
That is the main reason `txt2unix` exists in the first place.

## OPTIONS

* `-n` :
  ensure output ends in a line feed character

## EXAMPLES

Example with UNIX line endings, i.e., just passthrough.

```
$ ./txt2unix < corpus/nix.txt | xxd -g1
00000000: 74 68 65 0a 71 75 69 63 6b 0a 62 72 6f 77 6e 0a  the.quick.brown.
00000010: 66 6f 78 0a                                      fox.
```

Example with WINDOWS line endings.

```
$ ./txt2unix < corpus/win.txt | xxd -g1
00000000: 74 68 65 0a 71 75 69 63 6b 0a 62 72 6f 77 6e 0a  the.quick.brown.
00000010: 66 6f 78 0a                                      fox.
```

Example with legacy MAC line endings.

```
$ ./txt2unix < corpus/mac.txt | xxd -g1
00000000: 74 68 65 0a 71 75 69 63 6b 0a 62 72 6f 77 6e 0a  the.quick.brown.
00000010: 66 6f 78 0a                                      fox.
```

Example of a ridiculous mixed case.

```
$ cat corpus/win.txt corpus/nix.txt corpus/mac.txt | ./txt2unix | xxd -g1
00000000: 74 68 65 0a 71 75 69 63 6b 0a 62 72 6f 77 6e 0a  the.quick.brown.
00000010: 66 6f 78 0a 74 68 65 0a 71 75 69 63 6b 0a 62 72  fox.the.quick.br
00000020: 6f 77 6e 0a 66 6f 78 0a 74 68 65 0a 71 75 69 63  own.fox.the.quic
00000030: 6b 0a 62 72 6f 77 6e 0a 66 6f 78 0a              k.brown.fox.
```

## AUTHOR

Written by Billy Bob Brumley, D.Sc. (Tech.).

## REPORTING BUGS

GitLab: <https://gitlab.com/nisec/txt2unix/>

## COPYRIGHT

Copyright (c) 2022 Billy Bob Brumley, D.Sc. (Tech.).
MIT License: <https://gitlab.com/nisec/txt2unix/-/blob/master/LICENSE>

## SEE ALSO

Full documentation at: <https://gitlab.com/nisec/txt2unix/>
